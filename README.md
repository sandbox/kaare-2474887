
# About

Hide menu blocks on pages that aren't part of the menu itself.

Say you have a short guide with six nodes of type `page` in a custom menu. You
only want this menu to be visible when you are within one of these six pages.
That's what introvert refers to. It keeps the menu to itself, its own context.
Configure your block as usual and enable the option *Hide menu block outside of
menu*.

This module filters the visibility of the block prior to rendering, but after
the core visibility algorithm. That means you can configure the standard
visibility settings as usual and have the Menu introvert filter on top of that.
It's a good idea, to continue the example above, to only show the block on
content of type `page` in addition. This filters out the block in an earlier
stage.
