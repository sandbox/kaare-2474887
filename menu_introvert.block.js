;(function($, Drupal) {

Drupal.behaviors.menuIntrovert = {
  attach: function(context) {
    $('fieldset#edit-menu-introvert', context).drupalSetSummary(function(context) {
      var vals = [];
      vals.push($('[name="menu_introvert"]:checked', context).val() == 1 ? 'Hidden outside own context' : 'Not hidden');
      return vals.join(', ');
    });
  }
};

})(jQuery, Drupal);
